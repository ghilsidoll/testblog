/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alex
 */
@Entity
@Table(name = "groupuser_has_articles")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GroupuserHasArticles.findAll", query = "SELECT g FROM GroupuserHasArticles g")
    , @NamedQuery(name = "GroupuserHasArticles.findByGroupuserName", query = "SELECT g FROM GroupuserHasArticles g WHERE g.groupuserHasArticlesPK.groupuserName = :groupuserName")
    , @NamedQuery(name = "GroupuserHasArticles.findByArticlesId", query = "SELECT g FROM GroupuserHasArticles g WHERE g.groupuserHasArticlesPK.articlesId = :articlesId")})
public class GroupuserHasArticles implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GroupuserHasArticlesPK groupuserHasArticlesPK;
    @JoinColumn(name = "groupuser_name", referencedColumnName = "name", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Groupuser groupuser;

    public GroupuserHasArticles() {
    }

    public GroupuserHasArticles(GroupuserHasArticlesPK groupuserHasArticlesPK) {
        this.groupuserHasArticlesPK = groupuserHasArticlesPK;
    }

    public GroupuserHasArticles(String groupuserName, int articlesId) {
        this.groupuserHasArticlesPK = new GroupuserHasArticlesPK(groupuserName, articlesId);
    }

    public GroupuserHasArticlesPK getGroupuserHasArticlesPK() {
        return groupuserHasArticlesPK;
    }

    public void setGroupuserHasArticlesPK(GroupuserHasArticlesPK groupuserHasArticlesPK) {
        this.groupuserHasArticlesPK = groupuserHasArticlesPK;
    }

    public Groupuser getGroupuser() {
        return groupuser;
    }

    public void setGroupuser(Groupuser groupuser) {
        this.groupuser = groupuser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupuserHasArticlesPK != null ? groupuserHasArticlesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GroupuserHasArticles)) {
            return false;
        }
        GroupuserHasArticles other = (GroupuserHasArticles) object;
        if ((this.groupuserHasArticlesPK == null && other.groupuserHasArticlesPK != null) || (this.groupuserHasArticlesPK != null && !this.groupuserHasArticlesPK.equals(other.groupuserHasArticlesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.GroupuserHasArticles[ groupuserHasArticlesPK=" + groupuserHasArticlesPK + " ]";
    }
    
}
