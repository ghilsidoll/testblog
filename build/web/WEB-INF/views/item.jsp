<%-- 
    Document   : item
    Created on : 14.06.2022, 13:42:28
    Author     : alex
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="main">
     <aside class="leftAside">
        <h2>Велосипеды</h2>
        <ul>
            <li><a href="#">Горные велосипеды</a></li>
            <li><a href="#">Спортивные велосипеды</a></li>
            <li><a href="#">Городские велосипеды</a></li>
            <li><a href="#">Другие</a></li>
        </ul>
    </aside>
    <section>
        <article>
            <h1>Giant FastRoad SL 2 (2022)</h1>
            <div class="text-article">
                <img src="/../../image/test-1.jpg" width="320" height="179"/>
                <p><b>Тип велосипеда:</b> спортивный</p>
                <p><b>Стоимость аренды:</b> 200 р./час</p>
                <p><b>Описание:</b>
                    Фитнес велосипед Giant FastRoad SL 2 (2022) – базовая модель, 
                    которая предназначается для катания по городской местности. 
                    Обода оформлены утолщенными ребристыми покрышками Giant S-R3 AC, 
                    puncture protect, 700x32c. Задний переключатель Shimano Sora 
                    гарантирует, что можно будет выбрать подходящую передачу для 
                    любой ситуации на дороге. Жёсткая вилка Advanced-Grade 
                    Composite, alloy OverDrive steerer для активного катания. 
                    Модель седла Giant Approach выполнена с учётом инновационных 
                    разработок. Дисковые гидравлические тормоза Tektro HD-R280 
                    велосипеда останавливают его моментально, независимо от 
                    качества дорожного покрытия. Алюминиевая рама ALUXX SL-Grade 
                    Aluminum обеспечивает маневренность и надёжность всей 
                    конструкции. Уменьшенный вес шатунов FSA Omega, 34/50 позволяет 
                    точнее передавать нагрузку на каретку велосипеда. 
                </p>
                <h3>Характеристики: </h3>
                <p><b>Диаметр колеса:</b> 23 дюйма</p>
                <p><b>Кол-во скоростей:</b> 18</p>
                <p><b>Тормоза:</b> передние, задние</p>
                
            </div>
            <div class="fotter-article">
                <span class="read"><a href="javascript:void(0);">Подробнее...</a></span>
                <span class="date-article">Дата добавления: 10.06.2022</span>
            </div>
    </section>
</div>