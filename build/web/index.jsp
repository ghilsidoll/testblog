<%-- 
    Document   : index
    Created on : 14.06.2022, 9:31:36
    Author     : alex
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="main">
    <aside class="leftAside">
        <h2>Велосипеды</h2>
        <ul>
            <li><a href="#">Горные велосипеды</a></li>
            <li><a href="#">Спортивные велосипеды</a></li>
            <li><a href="#">Городские велосипеды</a></li>
            <li><a href="#">Другие</a></li>
        </ul>
    </aside>
    <section>
        <article>
            <h1>Giant FastRoad SL 2 (2022)</h1>
            <div class="text-article">
                <p><b>Тип велосипеда:</b> горный</p>
                <p>другие характеристики</p>
            </div>
            <div class="fotter-article">
                <span class="read"><a href="javascript:void(0);">Подробнее...</a></span>
                <span class="date-article">Дата добавления: 10.06.2022</span>
            </div>
        </article>
        <article>
            <h1>Bear Bike Prague (2021)</h1>
            <div class="text-article">
                <p><b>Тип велосипеда:</b> городской</p>
                <p>другие характеристики</p>
            </div>
            <div class="fotter-article">
                <span class="read"><a href="javascript:void(0);">Подробнее...</a></span>
                <span class="date-article">Дата добавления: 10.06.2022</span>
            </div>
        </article>
    </section>
</div>
